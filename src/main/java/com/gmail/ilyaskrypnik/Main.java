package com.gmail.ilyaskrypnik;

public class Main {
    public static void main(String[] args) {
        ParserApplication app = new ParserApplication(args);
        app.startApp();
        app.writeResult();
    }
}