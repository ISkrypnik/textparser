package com.gmail.ilyaskrypnik.file.handling;

import java.util.concurrent.atomic.AtomicInteger;

public final class ReadStatus {
    private final int amountOfFiles;
    private final AtomicInteger readCounter = new AtomicInteger(0);

    public ReadStatus(int amountOfFiles) {
        this.amountOfFiles = amountOfFiles;
    }

    void increaseValueOfReadFiles() {
        readCounter.addAndGet(1);
    }

    boolean areReadersFinished() {
        return readCounter.intValue() >= amountOfFiles;
    }
}