package com.gmail.ilyaskrypnik.file.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public final class Parser implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(Parser.class);

    private final ReadStatus readStatus;
    private final BlockingQueue<String> queue;
    private final Set<String> dictionary;
    private final List<Character> alphabet;

    public Parser(BlockingQueue<String> stringsToParse, Set<String> dictionary,
                  ReadStatus readStatus) {
        this.queue = stringsToParse;
        this.dictionary = dictionary;
        this.readStatus = readStatus;
        alphabet = getAlphabet();
    }

    List<Character> getAlphabet() {
        List<Character> ruCharacters = new ArrayList<>();
        for (int i = 1040; i <= 1103; i++) {
            ruCharacters.add((char) i);
        }
        return ruCharacters;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String stringFromQueue = queue.poll(2, TimeUnit.SECONDS);

                if (stringFromQueue == null || stringFromQueue.length() == 0) {
                    if (readStatus.areReadersFinished() && queue.isEmpty()) {
                        break;
                    }
                } else {
                    List<String> wordsForDictionary = getListOfWordsFromFilesString(stringFromQueue);
                    dictionary.addAll(wordsForDictionary);
                }
            }
        } catch (InterruptedException e) {
            log.error("Interrupted parser", e);
            Thread.currentThread().interrupt();
        }
    }

    List<String> getListOfWordsFromFilesString(String string) {
        List<String> words = new ArrayList<>();
        StringBuilder word = new StringBuilder();

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ' && string.charAt(i) != '.') {
                word.append(string.charAt(i));
            } else {
                if (isWordValid(word.toString())) {
                    words.add(word.toString().toLowerCase());
                }
                word.setLength(0);
            }
        }
        if (isWordValid(word.toString())) {
            words.add(word.toString().toLowerCase());
        }
        return words;
    }

    boolean isWordValid(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (!alphabet.contains(word.charAt(i))) {
                return false;
            }
        }
        return word.length() > 2;
    }
}