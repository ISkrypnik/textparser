package com.gmail.ilyaskrypnik.file.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class FileWriter {
    private static final Logger log = LoggerFactory.getLogger(FileWriter.class);

    public void writeToFile(Set<String> words, String outputPath) {
        try (PrintWriter out = new PrintWriter(outputPath)) {
            List<String> sortedWords = new ArrayList<>(words);
            Collections.sort(sortedWords);

            for (String word : sortedWords) {
                out.println(word);
            }
            log.info("Done. Total unique words count: {}", sortedWords.size());
        } catch (FileNotFoundException e) {
            log.error("Exception in FileWriter: ", e);
        }
    }
}