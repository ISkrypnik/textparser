package com.gmail.ilyaskrypnik.file.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;

public final class FileReader implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(FileReader.class);

    private final String pathToInputFile;
    private final BlockingQueue<String> unhandledStrings;
    private final ReadStatus readStatus;

    public FileReader(String path, BlockingQueue<String> strings, ReadStatus readStatus) {
        this.pathToInputFile = path;
        this.unhandledStrings = strings;
        this.readStatus = readStatus;
    }

    @Override
    public void run() {
        try (FileInputStream inputStream = new FileInputStream(pathToInputFile)) {
            try (Scanner scanner = new Scanner(inputStream)) {

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    unhandledStrings.put(line);
                }
                readStatus.increaseValueOfReadFiles();
            } catch (InterruptedException e) {
                log.error("Interrupted reader", e);
                Thread.currentThread().interrupt();
            }
        } catch (IOException e) {
            log.error("IOException while reading file", e);
        }
    }
}