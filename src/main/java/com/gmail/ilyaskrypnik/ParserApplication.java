package com.gmail.ilyaskrypnik;

import com.gmail.ilyaskrypnik.file.handling.FileReader;
import com.gmail.ilyaskrypnik.file.handling.FileWriter;
import com.gmail.ilyaskrypnik.file.handling.Parser;
import com.gmail.ilyaskrypnik.file.handling.ReadStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

final class ParserApplication {
    private static final Logger log = LoggerFactory.getLogger(ParserApplication.class);
    private static final int amountOfParsers = 2;

    private final int amountOfFiles;
    private final int amountOfThreads;
    private final List<String> filePaths;
    private final ConcurrentHashMap.KeySetView<String, Boolean> sharedDictionary;

    ParserApplication(String[] args) {
        sharedDictionary = ConcurrentHashMap.newKeySet();
        filePaths = Arrays.asList(args);
        amountOfFiles = filePaths.size();
        amountOfThreads = amountOfFiles + amountOfParsers;
    }

    void startApp() {
        ExecutorService pool = Executors.newFixedThreadPool(amountOfThreads - amountOfFiles);
        ExecutorService readingPool = Executors.newFixedThreadPool(amountOfFiles);

        ReadStatus readStatus = new ReadStatus(amountOfFiles);
        BlockingQueue<String> queue = new LinkedBlockingQueue<>();

        for (String path : filePaths) {
            readingPool.execute(new FileReader(path, queue, readStatus));
        }

        for (int i = 1; i <= amountOfParsers; i++) {
            pool.execute(new Parser(queue, sharedDictionary, readStatus));
        }

        stopThreads(readingPool);
        stopThreads(pool);
    }

    void writeResult() {
        FileWriter writer = new FileWriter();
        String outputPath = "dictionary.txt";
        writer.writeToFile(sharedDictionary, outputPath);
    }

    private void stopThreads(ExecutorService pool) {
        pool.shutdown();

        try {
            pool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("Interrupted exception while Stopping threads: ", e);
            Thread.currentThread().interrupt();
        } finally {
            pool.shutdownNow();
        }
    }
}
