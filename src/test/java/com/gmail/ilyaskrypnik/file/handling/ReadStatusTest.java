package com.gmail.ilyaskrypnik.file.handling;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReadStatusTest {
    private ReadStatus readStatus;

    @Before
    public void initReadStatus() {
        final int amountOfFiles = 4;
        readStatus = new ReadStatus(amountOfFiles);
    }

    @Test
    public void testRightIncreasedValueOfReadFiles() {
        for (int i = 0; i < 4; i++) {
            readStatus.increaseValueOfReadFiles();
        }
        Assert.assertTrue(readStatus.areReadersFinished());
    }

    @Test
    public void testWrongIncreasedValueOfReadFiles() {
        for (int i = 0; i < 3; i++) {
            readStatus.increaseValueOfReadFiles();
        }
        Assert.assertFalse(readStatus.areReadersFinished());
    }
}