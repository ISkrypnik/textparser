package com.gmail.ilyaskrypnik.file.handling;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class ParserTest {
    private Parser parser;

    @Before
    public void initParser() {
        final ConcurrentHashMap.KeySetView<String, Boolean> sharedDictionary = ConcurrentHashMap.newKeySet();
        final BlockingQueue<String> queue = new LinkedBlockingQueue<>();
        final ReadStatus readStatus = new ReadStatus(2);
        parser = new Parser(queue, sharedDictionary, readStatus);
    }

    @Test
    public void testParsedString() {
        String beforeParsing = "Василий. Женя. Алина Коля ";
        List<String> words = Arrays.asList("василий", "женя", "алина", "коля");

        Assert.assertEquals(words, parser.getListOfWordsFromFilesString(beforeParsing));
    }

    @Test
    public void testGettingRuAlphabet() {
        List<Character> rightRuAlphabet = new ArrayList<>();

        for (int i = 1040; i <= 1103; i++) {
            rightRuAlphabet.add((char) i);
        }

        Assert.assertEquals(rightRuAlphabet, parser.getAlphabet());
    }

    @Test
    public void testRightWordValidation() {
        String rightWord = "Человек";
        Assert.assertTrue(parser.isWordValid(rightWord));
    }

    @Test
    public void testShortWordValidation() {
        String shortWord = "из";
        Assert.assertFalse(parser.isWordValid(shortWord));
    }

    @Test
    public void testWordWithSymbolsValidation() {
        String wordWithSymbols = "Р2Д2";
        Assert.assertFalse(parser.isWordValid(wordWithSymbols));
    }
}